#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>

namespace gazebo
{
  class ModelPush : public ModelPlugin
  {

    public : physics::ModelPtr my_sphere_model;
    public: int update_counter = 0;

    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {

      sdf::SDF sphereSDF;
    sphereSDF.SetFromString(
       "<sdf version ='1.4'>\
          <model name ='sphere'>\
            <static>1</static>\
            <pose>2 0 2 0 0 0</pose>\
            <link name ='link'>\
              <pose>0 0 .5 0 0 0</pose>\
              <collision name ='collision'>\
                <geometry>\
                  <sphere><radius>0.5</radius></sphere>\
                </geometry>\
              </collision>\
              <visual name ='visual'>\
                <geometry>\
                  <sphere><radius>0.5</radius></sphere>\
                </geometry>\
              </visual>\
            </link>\
          </model>\
        </sdf>");


    sdf::ElementPtr model = sphereSDF.Root()->GetElement("model");
    model->GetAttribute("name")->SetFromString("unique_sphere");
    _parent->GetWorld()->InsertModelSDF(sphereSDF);


      this->model = _parent;
      // Store the pointer to the model
      std::cout << "Load() function is loaded " << std::endl;
      std::cout << "Number of links: " << this->model->GetLinks().size() << std::endl;
      std::cout << "Link 0 "  << this->model->GetLinks()[0]->GetName() <<std::endl;
      std::cout << "Link 1 "  << this->model->GetLinks()[1]->GetName() <<std::endl;
      std::cout << "Link 2 "  << this->model->GetLinks()[2]->GetName() <<std::endl;
      std::cout << "Link 3 "  << this->model->GetLinks()[3]->GetName() <<std::endl;



      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ModelPush::OnUpdate, this));
    }

    // Called by the world update start event
    public: void OnUpdate()
    {

      // my_sphere_model->SetLinearVel(ignition::math::Vector3d(.3, 0, 0));
      std::cout << "Here on Update" << std::endl;
      my_sphere_model = this->model->GetWorld()->ModelByName("unique_sphere");

      update_counter+= 1 ;
      my_sphere_model->SetWorldPose(ignition::math::Pose3d(5, 0, 2, 0,0,0),true,true);

      if (update_counter > 100) {
        my_sphere_model->SetWorldPose(ignition::math::Pose3d(-5, 0, 3, 0,0,0),true,true);

      }

      for (int i = 0 ; i< this->model->GetLinks().size(); i++){

      ignition::math::Vector3d link_pos (this->model->GetLinks()[i]->WorldPose().Pos().X(),this->model->GetLinks()[i]->WorldPose().Pos().Y(),this->model->GetLinks()[i]->WorldPose().Pos().Z());
      ignition::math::Quaterniond link_quaternion (this->model->GetLinks()[i]->WorldPose().Rot().W(),this->model->GetLinks()[i]->WorldPose().Rot().X(),this->model->GetLinks()[i]->WorldPose().Rot().Y(),this->model->GetLinks()[i]->WorldPose().Rot().Z());
      ignition::math::Pose3d link_position_3d (link_pos,link_quaternion);

      std::cout << this->model->GetLinks()[i]->GetName()<< std::endl;
      std::cout << link_position_3d << std::endl;
      uint32_t visual_id;

      this->model->GetLinks()[i]->VisualId("visual",visual_id);
      // std::cout << visual_id << std::endl;
      ignition::math::Pose3d test_pose;
      this->model->GetLinks()[i]->VisualPose(visual_id ,test_pose);
      // std::cout << test_pose << std::endl;



      }


    }

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}