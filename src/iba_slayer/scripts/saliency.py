#!/usr/bin/env python

import os
import rospy
from external_module_interface.external_module import ExternalModule
from std_msgs.msg import String

import sys
CURRENT_TEST_DIR = os.getcwd()
sys.path.append(CURRENT_TEST_DIR + "/../../src")
sys.path.append("/home/erdi/Desktop/Storage/NRP/slayerPytorch/src/")
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch.utils.data import Dataset, DataLoader
import slayerSNN as snn
from learningStats import learningStats
import zipfile

netParams = snn.params('/home/erdi/Documents/NRP/GazeboRosPackages/src/iba_slayer/scripts/network.yaml')

class nmnistDataset(Dataset):
    def __init__(self, datasetPath, sampleFile, samplingTime, sampleLength):
        self.path = datasetPath
        self.samples = np.loadtxt(sampleFile).astype('int')
        self.samplingTime = samplingTime
        self.nTimeBins = int(sampleLength / samplingTime)

    def __getitem__(self, index):
        inputIndex = self.samples[index, 0]
        classLabel = self.samples[index, 1]

        inputSpikes = snn.io.read2Dspikes(
            self.path + str(inputIndex.item()) + '.bs2'
        ).toSpikeTensor(torch.zeros((2, 34, 34, self.nTimeBins)),
                        samplingTime=self.samplingTime)
        desiredClass = torch.zeros((10, 1, 1, 1))
        desiredClass[classLabel, ...] = 1
        # Input spikes are reshaped to ignore the spatial dimension and the neurons are placed in channel dimension.
        # The spatial dimension can be maintained and used as it is.
        # It requires different definition of the dense layer.
        return inputSpikes.reshape(
            (-1, 1, 1, inputSpikes.shape[-1])), desiredClass, classLabel

    def __len__(self):
        return self.samples.shape[0]

# Network definition
class Network(torch.nn.Module):
    def __init__(self, netParams):
        super(Network, self).__init__()
        # Initialize slayer
        slayer = snn.layer(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        # Define network functions
        # The commented line below should be used if the input spikes were not reshaped
        # self.fc1   = slayer.dense((34, 34, 2), 512)
        self.fc1 = slayer.dense((34 * 34 * 2), 512)
        self.fc2 = slayer.dense(512, 10)

    def forward(self, spikeInput):
        # Both set of definitions are equivalent. The uncommented version is much faster.

        # spikeLayer1 = self.slayer.spike(self.fc1(self.slayer.psp(spikeInput)))
        # spikeLayer2 = self.slayer.spike(self.fc2(self.slayer.psp(spikeLayer1)))

        spikeLayer1 = self.slayer.spike(self.slayer.psp(self.fc1(spikeInput)))
        spikeLayer2 = self.slayer.spike(self.slayer.psp(self.fc2(spikeLayer1)))

        return spikeLayer2

class SaliencyModule(ExternalModule):
    def __init__(self, module_name=None, steps=1):
        super(SaliencyModule, self).__init__(module_name, steps)

    def initialize(self):
        rospy.loginfo("initialize")
        self.device = torch.device('cuda')
        # Dataset and dataLoader instances.
        trainingSet = nmnistDataset(
            datasetPath=netParams['training']['path']['in'],
            sampleFile=netParams['training']['path']['train'],
            samplingTime=netParams['simulation']['Ts'],
            sampleLength=netParams['simulation']['tSample'])
        self.trainLoader = DataLoader(dataset=trainingSet, batch_size=8, shuffle=False,
                                    num_workers=4)

        self.testingSet = nmnistDataset(datasetPath=netParams['training']['path']['in'],
                                    sampleFile=netParams['training']['path'][
                                        'test'],
                                    samplingTime=netParams['simulation']['Ts'],
                                    sampleLength=netParams['simulation']['tSample'])
        self.testLoader = DataLoader(dataset=self.testingSet, batch_size=1, shuffle=False,
                                    num_workers=4)

        # Learning stats instance.
        stats = learningStats()                            
        
        self.net = Network(netParams).to(self.device)
        module = self.net  #multiGpu can use module=net.module
        checkpoint = torch.load("/home/erdi/Desktop/Storage/NRP/slayerPytorch/Logs_MLP/checkpoint90.pt")
        module.load_state_dict(checkpoint['net'])

        self.prediction_pub = rospy.Publisher("prediction",String,queue_size=10)

        self.testloader_iterator = enumerate(self.testLoader)
        
    def run_step(self):
        # rospy.loginfo("run_step")
        try:
            i,(input, target, label) = next(self.testloader_iterator)
            input = input.to(self.device)

            output = self.net.forward(input)
            
            prediction_output= snn.predict.getClass(output)
            string_output  = str(prediction_output.cpu().detach().numpy()[0])
            print(string_output)
            print(type(string_output))
            
        except:
            string_output = "N/A"
            rospy.loginfo("except")

        self.prediction_pub.publish(string_output)


if __name__=='__main__':
    resampling_module = SaliencyModule(module_name='module1', steps=1)
    rospy.loginfo("main")
    rospy.spin()

