
## The experiment is tested using NRP 3.1.0
```bash
roslaunch shadow_hand_contact_sensor shadow_hand_gazebo.launch
```

This will start shadow hand with contact sensors.


```bash
roslaunch sr_description shadow_hand_gazebo.launch
```

This will start vanilla shadow hand


You can activate the controller using above command after you start the shadow hand.

## Model Architrecture![Model Architecture](https://i.imgur.com/aGGiY2j.png)

