#!/bin/bash
set -e
set -o
set -x

# Debug info
lsb_release -a

# # The first parameter should be location of user_scripts
# USER_SCRIPTS=${1}

# # Source fixed nrp_variables. The file sourced from bashrc doesn't correctly recognise Jenkins shell process
# # and GazeboRosPackages aren't initialized correctly

# if [ -z ${USER_SCRIPTS} ]; then
#     echo "USAGE: The path to user-scripts should be specified as the first parpameter"
#     exit 1
# else
#     . ${USER_SCRIPTS}/nrp_variables
# fi

# Update git sunmodules
git submodule init && git submodule update

# 
source /opt/ros/noetic/setup.bash

# Build GazeboRosPackages
# rm -rf build devel
catkin build
